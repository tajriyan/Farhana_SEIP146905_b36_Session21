<?php
namespace App\Model;
use PDO; //PDO is a built in class
use PDOException; //PDOException is a built in class

class Database{

    public $DBH;
    public $host = "localhost";
    public $dbname = "atomic_project_b36";
    public $user = "root";
    public $password = "";

    public function __construct(){

      try{
        $this->DBH =new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
        echo "Connection Successfull<br>";
      }
      catch(PDOException $error){

          $error->getMessage();

      }
    }
}// end of database class
